# Set Promtail
### 1. เปลี่ยน hostname, log_type ใน config.yml
### 2. เปลี่ยน path ของ log ใน docker-compose.yml

### ตัวอย่าง Query ที่เอา 2 Host
```
{filename=~"/var/log/.*", hostname=~"mac-korkla|test-mac-korkla"}
```

### สามารถเพิ่ม Target ได้เรื่อยๆ
```
- job_name: system
  static_configs:
  - targets:
      - localhost
    labels:
      job: varlogs
      __path__: /var/log/*log
      hostname: mac-korkla
  - targets:
      - localhost
    labels:
      job: varlogs
      __path__: /var/log2/*log
      hostname: mac-korkla-2
```


### Folder ของ Grafana, prometheus และ Loki  ต้อง chmod
```
sudo chmod 777 grafana/data
```
```
sudo chmod 777 prometheus/data
```
```
sudo chmod 777 loki/data
```

### Reload config Prometheus
```
curl --request POST http://localhost:9090/-/reload
```
ถ้าไม่ได้
```
docker restart prometheus
```